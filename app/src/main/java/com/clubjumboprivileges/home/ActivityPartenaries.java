package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityPartenaries extends AppCompatActivity {

    ProgressDialog progressDialog;
    WebView webPartenaries;
    ImageView backBtn;
    TextView textHeader;
    RelativeLayout relativeNointernet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partenaries);

        progressDialog=new ProgressDialog(this);
        webPartenaries = findViewById(R.id.webPartenaries);
        backBtn=(ImageView)findViewById(R.id.backBtn);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        textHeader = findViewById(R.id.textHeader);
        textHeader.setText(R.string.nos_partenaries);
        initializeListener();

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#000000"));
            }
            getSupportActionBar().hide();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        getData();

    }

    private void initializeListener() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webPartenaries.canGoBack()){
                    webPartenaries.goBack();
                }
                else{
                    onBackPressed();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (webPartenaries.canGoBack()){
            webPartenaries.goBack();
        }
        else{
            super.onBackPressed();
        }
    }

    private void getData() {
        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webPartenaries.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webPartenaries.loadUrl("https://jumbo.mu/my-jumbo-le-club/nos-partenaires-minipage");
            WebSettings webSettings = webPartenaries.getSettings();
            webSettings.setLoadsImagesAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            webPartenaries.setWebViewClient(new ActivityPartenaries.webviewfragement());
            webPartenaries.setWebChromeClient(new WebChromeClient());
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webPartenaries.setVisibility(View.GONE);

            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }


    }

    public class webviewfragement extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }
    public final boolean isInternetOn() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
