package com.clubjumboprivileges.home;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;


public class ActivitySplash extends Activity {

    SharedPreferences pref;
    SharedPreferences sharedpreferencesBarcode;
    Editor edit;
    Intent intent;
    int id;
    boolean isFromNotification=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for sInstance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        sharedpreferencesBarcode = getSharedPreferences("BAR", Context.MODE_PRIVATE);
        pref = getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
        edit = pref.edit();
        if (getIntent() != null) {
            Intent intent = getIntent();
             isFromNotification= intent.getBooleanExtra("isFromNotification", false);
        }

        if (isFromNotification) {
            Intent intent2 = new Intent(ActivitySplash.this, ActivityPromotions.class);
            startActivity(intent2);
            finish();
        } else {
            int SPLASH_TIME_OUT = 2000;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Log.e("StatusStatus", "StatusStatusStatus" +
                            pref.getBoolean("Status", false));
                    Intent i = new Intent(ActivitySplash.this, ActivityNavigation.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }
}
