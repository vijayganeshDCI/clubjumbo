package com.clubjumboprivileges.home;

import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ActivityIntroduction extends Activity {

	ViewPager viewPager;
	MyPagerAdapter myPagerAdapter;
	RelativeLayout page1;
	RelativeLayout mainlayout;
	Animation animBounce;
	ImageView img1;
	ImageView img2;
	ImageView img3;
	ImageView img4;
	ImageView img5;
	ImageView img6;
	ImageView img7;
	ImageView changeimage;
	ImageView changeimagebottom;
	Editor edit;
	Button skip;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.introductionswiper);

		viewPager = (ViewPager)findViewById(R.id.pager);
		img1 = (ImageView)findViewById(R.id.dot1);
		img2 = (ImageView)findViewById(R.id.dot2);
		img3 = (ImageView)findViewById(R.id.dot3);	
		img4 = (ImageView)findViewById(R.id.dot4);	
		img5 = (ImageView)findViewById(R.id.dot5);
		img6 = (ImageView)findViewById(R.id.dot6);
		img7 = (ImageView)findViewById(R.id.dot7);

		myPagerAdapter = new MyPagerAdapter();
		viewPager.setAdapter(myPagerAdapter);	
		
		img1.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
		img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
		
		viewPager.addOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				if(arg0==0)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==1)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));	
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==2)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));	
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==3)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));	
					img4.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==4)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));	
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==5)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
				}
				else if(arg0==6)
				{
					img1.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img2.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img3.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img4.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img5.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img6.setImageDrawable(getResources().getDrawable(R.drawable.lightdot));
					img7.setImageDrawable(getResources().getDrawable(R.drawable.darkdot));
				}
				
			}
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	
	}

	private class MyPagerAdapter extends PagerAdapter{

		int numberOfPages = 7;

		@Override
		public int getCount() {
			return numberOfPages;
		}

		@Override
		public boolean isViewFromObject(View view,Object object) {
			return view == object;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			LayoutInflater mInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View view = mInflater.inflate(R.layout.introductionscreen, null);

			page1 = (RelativeLayout)view.findViewById(R.id.pager1);
			changeimage = (ImageView)view.findViewById(R.id.changeimage);
			changeimagebottom = (ImageView)view.findViewById(R.id.changeimagebottom);
			animBounce = AnimationUtils.loadAnimation(ActivityIntroduction.this,
					R.anim.slide_down);		
			skip = (Button)view.findViewById(R.id.skipbutton);
			if(position==0)
			{
				changeimage.setImageResource(R.drawable.instrutions1);
				changeimage.setVisibility(View.VISIBLE);
				changeimagebottom.setVisibility(View.GONE);
			}else if(position==1)
			{
				changeimage.setImageResource(R.drawable.instrutions2);
				changeimage.setVisibility(View.VISIBLE);
				changeimagebottom.setVisibility(View.GONE);

			}else if(position==2)
			{
				changeimage.setImageResource(R.drawable.instrutions3);
				changeimagebottom.setVisibility(View.GONE);
				changeimage.setVisibility(View.VISIBLE);
			}
			else if(position==3)
			{
				changeimage.setImageResource(R.drawable.instrutions4);
				changeimagebottom.setVisibility(View.GONE);
				changeimage.setVisibility(View.VISIBLE);
			}
			else if(position==4)
			{
				changeimage.setImageResource(R.drawable.instrutions5);
				changeimagebottom.setVisibility(View.GONE);
				changeimage.setVisibility(View.VISIBLE);
			}
			else if(position==5)
			{
				changeimage.setImageResource(R.drawable.instrutions6);
				changeimagebottom.setVisibility(View.GONE);
				changeimage.setVisibility(View.VISIBLE);
			}
			else if(position==6)
			{
				changeimage.setImageResource(R.drawable.instrutions7);
				changeimagebottom.setVisibility(View.GONE);
				changeimage.setVisibility(View.VISIBLE);
			}

			skip.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});

			container.addView(view);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((LinearLayout)object);
		}

	}

}
