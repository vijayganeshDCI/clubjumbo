package com.clubjumboprivileges.home;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityEmailsign extends AppCompatActivity {

    ImageView backBtn;
    ProgressDialog progressDialog;
    WebView webViewEmailsign;
    TextView textHeader;
    RelativeLayout relativeNointernet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emailsign);

        progressDialog = new ProgressDialog(this);
        backBtn = findViewById(R.id.backBtn);
        textHeader = findViewById(R.id.textHeader);
        webViewEmailsign = findViewById(R.id.web_email);
        relativeNointernet = findViewById(R.id.relativeNointernet);
        textHeader.setText(R.string.app_name);

        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor("#000000"));
            }
            getSupportActionBar().hide();
        }
        catch (Exception e){
            e.printStackTrace();
        }


        if(isInternetOn()) {

            relativeNointernet.setVisibility(View.GONE);
            webViewEmailsign.setVisibility(View.VISIBLE);

            progressDialog.setMessage(getString(R.string.loading));
            webViewEmailsign.loadUrl("https://jumbo.mu/subscriberminipage?email=1");
            WebSettings webSettings = webViewEmailsign.getSettings();
            webSettings.setJavaScriptEnabled(true);
            progressDialog.show();
            webViewEmailsign.setWebViewClient(new WebviewEmailSign());
        }
        else
        {
            relativeNointernet.setVisibility(View.VISIBLE);
            webViewEmailsign.setVisibility(View.GONE);
            Toast.makeText(this,  R.string.no_internet, Toast.LENGTH_SHORT).show();
        }



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webViewEmailsign.canGoBack()){
                    webViewEmailsign.goBack();
                }
                else{
                    onBackPressed();
                }
            }
        });
    }
        public class WebviewEmailSign extends WebViewClient
        {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        }


    @Override
    public void onBackPressed() {
        if (webViewEmailsign.canGoBack()){
            webViewEmailsign.goBack();
        }
        else{
            super.onBackPressed();
        }
    }

    public final boolean isInternetOn() {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    }


