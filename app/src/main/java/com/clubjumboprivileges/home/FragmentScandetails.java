package com.clubjumboprivileges.home;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.utils.CheckingNetworkAvailable;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class FragmentScandetails extends Fragment{

	EditText firstname;
	ImageView barcodeimg;
	TextView scanNumber;
	SharedPreferences sharedpreferencesBarcode;
	Button save;
	String barcode = "";
	String barcodeFormat ="";
	DatabaseHandler dbh;
	SharedPreferences pref;
	Editor edit;
	CheckingNetworkAvailable check;
	LinearLayout commonlayout;
	ImageView backBtn;
	TextView textHeader;

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_scandetails, container, false);
		init(rootView);
		sharedpreferencesBarcode = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
		backBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().onBackPressed();
			}
		});

		String name="";
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			 name= bundle.getString("cont");

		}

		Log.e("value","->"+name);
		scanNumber.setText(""+name);// Key, default value
		Intent in=new Intent("settype");
		in.putExtra("name",1);
		getActivity().sendBroadcast(in);
		pref = getActivity().getSharedPreferences("LOYALTY", Context.MODE_PRIVATE);
		edit = pref.edit();
		barcode  = pref.getString("CONTENTS", "");
		barcodeFormat = pref.getString("FORMAT", "");
		final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		final Date date = new Date();
		commonlayout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				View view = getActivity().getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				}	
			}
		});
		save.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {

				String fname = firstname.getText().toString().trim();
				View view = getActivity().getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				}	
				if(fname.equals(""))
				{

					Toast.makeText(getActivity(), "Le champ du code à barres ne doit pas être vide", Toast.LENGTH_SHORT).show();
				}else{
					try {
						dbh.opendb();
						dbh.Deleterecord();
						dbh.insert_data(firstname.getText().toString(), barcode, dateFormat.format(date), barcodeFormat);
						dbh.closeconnection();
						Editor editor = sharedpreferencesBarcode.edit();
						editor.putInt("check", 5);
						editor.commit();
						getActivity().sendBroadcast(new Intent("scan"));
						Intent intent = new Intent(getActivity(), ActivityNavigation.class);// New activity
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						getActivity().finish();
					}
					catch (Exception e){
						e.printStackTrace();
					}

				}
				
			}
		});

		Bitmap bitmap = null;	   
		int height = 600;
		int width = 300;
		Log.e("barcode","barcodebarcode"+ barcode);
		scanNumber.setText(""+barcode);
		try {
			BarcodeEncoder barcodeEncoder = new BarcodeEncoder();

			if(!barcodeFormat.equals(""))
			{
				if(barcodeFormat.equalsIgnoreCase("AZTEC"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.AZTEC, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("CODABAR"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.CODABAR, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("CODE_39"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.CODE_39, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("CODE_93"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.CODE_93,height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("CODE_128"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.CODE_128, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("DATA_MATRIX"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.DATA_MATRIX, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("EAN_8"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.EAN_8, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("EAN_13"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.EAN_13, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("ITF"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.ITF, height, width);
					barcodeimg.setImageBitmap(bitmap);	
				}else if(barcodeFormat.equalsIgnoreCase("MAXICODE"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.MAXICODE, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}
				else if(barcodeFormat.equalsIgnoreCase("PDF_417"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.PDF_417, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("QR_CODE"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.QR_CODE, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("RSS_14"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.RSS_14, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("RSS_EXPANDED"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.RSS_EXPANDED, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("UPC_A"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.UPC_A, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("UPC_E"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.UPC_E, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}else if(barcodeFormat.equalsIgnoreCase("UPC_EAN_EXTENSION"))
				{
					bitmap = barcodeEncoder.encodeBitmap(barcode, BarcodeFormat.UPC_EAN_EXTENSION, height, width);
					barcodeimg.setImageBitmap(bitmap);
				}


			}

		} catch (WriterException e) {
			e.printStackTrace();
		}
		return rootView;
	}
	private void init(View view) {
		check = new CheckingNetworkAvailable(getActivity());
		commonlayout = (LinearLayout)view.findViewById(R.id.commonlayout);
		scanNumber =(TextView)view.findViewById(R.id.scanum);
		firstname = (EditText)view.findViewById(R.id.firstname);
		barcodeimg = (ImageView)view.findViewById(R.id.barcodeimg);
		save = (Button)view.findViewById(R.id.save);
		dbh = new DatabaseHandler(getActivity());
		check.setButtontypeface(4, save);
		backBtn=(ImageView)view.findViewById(R.id.backBtn);
		textHeader = view.findViewById(R.id.textHeader);
		textHeader.setText(R.string.personalisation);
	}

}