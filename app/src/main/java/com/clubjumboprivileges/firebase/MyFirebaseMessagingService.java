package com.clubjumboprivileges.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.clubjumboprivileges.home.ActivitySplash;
import com.clubjumboprivileges.home.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        getNotification(remoteMessage);
    }

    private void getNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, ActivitySplash.class);
        intent.putExtra("isFromNotification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Club_Jumbo";
        String channelName = "Common Notification";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo_notifi);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        notificationBuilder.setContentTitle(remoteMessage.getData().get("title")!=null?remoteMessage.getData().get("title"):"");
        notificationBuilder.setContentText(remoteMessage.getData().get("text")!=null?remoteMessage.getData().get("text"):"");
        notificationBuilder.setAutoCancel(true);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(sound);
        notificationBuilder.setLargeIcon(largeIcon);
        notificationBuilder.setSmallIcon(R.mipmap.logo_notifi);
        notificationBuilder.setContentIntent(pendingIntent);

        int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(notificationID, notificationBuilder.build());
    }
}