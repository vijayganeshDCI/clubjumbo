package com.clubjumboprivileges.utils;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.clubjumboprivileges.database.DatabaseHandler;
import com.clubjumboprivileges.home.ActivityNavigation;
import com.clubjumboprivileges.home.R;


public class Alertdialog {

	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;
	DatabaseHandler dbh;
	public Alertdialog(final Activity con,final DrawerLayout mDrawerLayout) {
		// TODO Auto-generated constructor stub

		dbh = new DatabaseHandler(con);

		final Dialog alertDialog = new Dialog(con);
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setContentView(R.layout.customalert);
		alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		alertDialog.show();
		final  Button Yes = (Button)alertDialog.findViewById(R.id.Yes);
		final Button  No = (Button)alertDialog.findViewById(R.id.no); 
		TextView titletext = (TextView)alertDialog.findViewById(R.id.titletext);

		titletext.setText("�tes-vous sur de  vouloir supprimer votre carte ?");

		Yes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dbh.opendb();
				dbh.Deleterecord();
				dbh.closeconnection();	
				alertDialog.dismiss();
				Intent i = new Intent(con,ActivityNavigation.class);
				con.startActivity(i);
				alertDialog.closeOptionsMenu();


			}
		});
		No.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				mDrawerLayout.closeDrawer(Gravity.LEFT);
				alertDialog.dismiss();					

			}
		});				


	}

}


